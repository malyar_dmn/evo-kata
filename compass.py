def directon(facing: str, turn: int):
    directions = ['N', 'NE', 'E', 'SE', 'S', 'SW', 'W', 'NW']
    deg = 45

    if (type(facing) is not str):
        raise TypeError('Facing argument must be a string')
    if (type(turn) is not int):
        raise TypeError('Turn argument must be an integer')

    new_direction = turn // deg
    facing_direction = directions.index(facing)
    result_direction = (facing_direction + new_direction) % len(directions)

    return directions[result_direction]


res = directon('S', 90)
print(res)
